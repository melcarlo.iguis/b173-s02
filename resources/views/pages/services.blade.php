@extends('layouts.app')
    
    @section('content')
        <h1>{{$title}}</h1>
        <p>This is the service section</p>
        @foreach ($services as $service)
            <li>{{ $service }}</li>
        @endforeach
    @endsection

