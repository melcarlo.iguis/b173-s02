@extends('layouts.app')

@section('content')
	<h1>{{ $post->title}}</h1>
	<small>Writen on{{$post->created_at}}</small>
	<p>{{$post->body}}</p>
	<hr>
	<a class="btn btn-primary" href="/posts/{{ $post->id }}/edit">Edit</a>
	    <hr>
	    
    <form method ="POST" action="/posts/{{$post->id}}">
    @csrf
    <input type="hidden" name="_method" value="DELETE">
    <button class="btn btn-danger">DELETE</button>
    </form>
@endsection