@extends('layouts.app')


@section('content')
	<h1>Edit</h1>
	<form method="POST" action="/posts/{{ $post->id }}">
		@csrf
		<!-- spoofing -->
		<input type="hidden" name="_method" value="PUT">
	  <div class="form-group">
	    <label for="title">Title</label>
	    <input value="{{ $post->title }}" type="text" class="form-control" name="title">
	  </div>

	  <div class="form-group">
	    <label for="body">Body</label>
	    <textarea class="form-control" rows="3" name="body" >{{ $post->body }}</textarea>
	  </div>
	  <button class="btn btn-primary" >Update</button>
	</form>
@endsection